// Function trial using GO
package main

import "fmt"

func main() {
	array1 := [5]int{2, 5, 7}
	var array2 = [5]int{1, 4, 8}

	if array1 == array2 {
		fmt.Println("array 1 and array 2 are equal")
	} else {
		fmt.Println("array are not equal")
	}

	fmt.Println(array1)

	for i := 0; i < len(array1); i++ {
		fmt.Printf("%d", array1[i])
	}
}

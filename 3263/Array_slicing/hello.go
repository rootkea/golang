package main

import (
	"fmt"
	"time"
)

const const1 = 53

type apple int8

// func main() {
// 	//var msg = "Hello World!"
// 	msg := "golang"
// 	fmt.Println(msg)
// 	fmt.Println(const1)
// 	var bar apple
// 	//	var bar int8
// 	fmt.Println(bar)
// 	p := func1()
// 	fmt.Println(*p)
// 	*p++
// 	fmt.Println(*p)
// }

// func func1() *int {
// 	var a int = 5
// 	return &a
// }

// first TASK-1: to declare array

// func main()  {
// 	array1 := [5]int{4,5,6}
// 	fmt.Println(array1)
// }

// declaring another array
// func main()  {
// 	array1 := [5]int{4,5,6}
// 	var array2 = [5]int{4,7,8}
// 	fmt.Println(array1)
// 	fmt.Println(array2)
// }

// Use of IF Else in Go

// func main()  {
// 	array1 := [5]int{4,5,6}
// 	var array2 = [5]int{4,7,8}
// 	if array1 == array2{
// 		fmt.Println("Both the arrays are of equal length")
// 	} else {       // this is the syntax rememeber to put the else just after this
// 		fmt.Println("Not equal....")
// 	}
// }

// Loops
// there is only FOR loop in GOlang
// func main() {
// 	array1 := [5]int{4, 5, 6}
// 	var array2 = [5]int{4, 7, 8}
// 	if array1 == array2 {
// 		fmt.Println("Both the arrays are of equal length")
// 	} else { // this is the syntax rememeber to put the else just after this
// 		fmt.Println("Not equal....")
// 	}

// 	for i := 0; i < len(array1); i++ {
// 		fmt.Printf("%d", array1[i])
// 	}
// }

// slice in Golang
// func main() {
// 	array1 := [5]int{4, 5, 6}
// 	slice1 := []int{1, 2}

// 	fmt.Println(array1)
// 	fmt.Println(slice1)

// 	slice1 = append(slice1, 6)
// 	fmt.Println(slice1)
// }

// to declare the hash map
// func main() {
// 	map1 := map[string]int{
// 		"foo": 2,
// 		"bar": 45,
// 	}

// 	fmt.Println(map1)
// }

// go routines
// func main() {
// 	slice := []int{1, 2, 3, 4, 5, 6, 8, 9}
// 	ans := sum(slice)
// 	fmt.Println(ans)
// }
// func sum(s []int) int {
// 	var ans int
// 	for _, v := range s {
// 		ans += v
// 	}
// 	return ans
// }

// Go routines-- its just a first party thread
// used whenever we need to divide the task and need concurrency we use go routines
func main() {
	go hello()

	fmt.Println("Main Done!")
	time.Sleep(2 * time.Second) // will wait for 2 seconds till that will print
}
func hello() {
	fmt.Println("HI")
	time.Sleep(1 * time.Second) // will wait for 1 second
}

// wait group are simply counters

// write a code to calculate the factorial of the number
// binary search -- take an array :and apply binary search
